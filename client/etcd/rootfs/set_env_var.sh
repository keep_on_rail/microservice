#!/bin/bash
random_uuid="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)"
export UUID=$random_uuid
echo $UUID
