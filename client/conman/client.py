import os
import socket
import json

import falcon
import etcd


# ETCD_HOST = 'etcd'

ETCD_HOST = os.environ.get('ETCD_HOST')
ETCD_PORT = 2379

etcd_client = etcd.Client(host=ETCD_HOST, port=ETCD_PORT)


# etcd_watcher = Program(key='/msg', filename='logs.txt', host=ETCD_HOST, port=ETCD_PORT)

# TODO: регистрация service_name:ip

class State:
    def on_get(self, req, resp):
        try:
            value = etcd_client.get('/msg').value
            resp.body = value
        except etcd.EtcdKeyNotFound:
            resp.body = 'There is no messages \n'

    def on_put(self, req, resp):
        if 'key' in req.params:
            key = req.params['key']
        else:
            key = '/msg'

        if 'value' in req.params:
            value = req.params['value'] + '\n'
        else:
            value = 'undefined'

        etcd_client.write(key, value)
        resp.body = falcon.HTTP_200


class Members:
    def on_get(self, req, resp):
        resp.body = json.dumps(etcd_client.members)


class Hostname:
    def on_get(self, req, resp):
        resp.body = socket.gethostname()


class NodeIP:
    def on_get(self, req, resp):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
        s.close()
        resp.body = ip


api = falcon.API()
api.add_route('/get_state', State())
api.add_route('/put', State())
api.add_route('/get_members', Members())
api.add_route('/get_hostname', Hostname())
api.add_route('/get_ip', NodeIP())
